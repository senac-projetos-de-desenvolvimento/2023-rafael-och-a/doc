# Documentação
Este repositório tem como objetivo armazenar toda a documentação pertinente ao projeto. Para mais informações, clique [aqui](https://gitlab.com/senac-projetos-de-desenvolvimento/2023-rafael-och-a/doc/-/wikis/home)

#Artigo
O artigo encontra-se em: https://medium.com/@rafaelochoamello/salva-obra-unindo-clientes-e-profissionais-da-%C3%A1rea-de-constru%C3%A7%C3%A3o-civil-2564d228e909